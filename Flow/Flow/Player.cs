﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Flow.EventHandlers;
using Flow.Utilities;

namespace Flow
{
    public static class Player
    {
        #region Properties

        static bool IsClicking { get; set; }
        public static Point CurrentPoint { get; set; }

        public static Color Color { get; set; }

        #endregion

        #region Constructors

        static Player()
        {
            IsClicking = false;
            CurrentPoint = Point.Zero;
            Color = Color.Transparent;
        }

        #endregion

        #region Events
        
        public static event ClickedHandler MouseUp;
        public static event ClickedHandler MouseDown;

        #endregion

        #region Methods

        public static void Update()
        {
            CurrentPoint = new Point(Utils.currMouse.X, Utils.currMouse.Y);
            //Point prevPoint = new Point(Utils.prevMouse.X, Utils.prevMouse.Y);

            IsClicking = Utils.currMouse.LeftButton == ButtonState.Pressed;
            bool wasClicking = Utils.prevMouse.LeftButton == ButtonState.Pressed;

            // TODO: agregar complejidad, si el mouse se sale de la pantalla entonces false.
            if (IsClicking != wasClicking)
            {
                if (IsClicking)
                    MouseDown(new ClickingEventArgs(IsClicking));
                else
                    MouseUp(new ClickingEventArgs(IsClicking));
            }
        }

        public static void Draw()
        {
            Utils.spriteBatch.Draw(Utils.whitePixel, new Rectangle(CurrentPoint.X - 2, CurrentPoint.Y - 2, 5, 5), Color.White);
        }

        #endregion
    }
}
