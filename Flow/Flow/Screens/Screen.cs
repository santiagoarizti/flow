﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flow.Screens
{
    public class Screen
    {
        #region Properties

        /// <summary>
        /// Absolute size in pixels, it is the size of the whole game.
        /// </summary>
        public Point Size { get; set; }

        /// <summary>
        /// Defaults to false. It tells the screen to work or not. Also, only one screen
        /// must be enabled at a time. It should be the visible screen.
        /// </summary>
        public bool Enabled { get; private set; }

        /// <summary>
        /// The color with which to fill the game background.
        /// </summary>
        public Color BackgroundColor { get; set; }

        /// <summary>
        /// The name of the screen, to aid in knowing what it does.
        /// </summary>
        public string Name { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Use default values to initialize screen.
        /// </summary>
        public Screen(bool enabled = false)
        {
            Size = new Point(500, 600);
            BackgroundColor = Color.Black;
            Name = "Unnamed screen";

            // If enabled, initialize
            Enabled = enabled;

            if (Enabled) Initialize();
        }

        #endregion

        #region Methods

        public void Enable()
        {
            foreach (var s in Screens) s.Value.Enabled = false;
            Enabled = true;
            Initialize();
        }

        public virtual void Update(GameTime gameTime)
        {
            
        }

        public virtual void Draw(GameTime gameTime)
        {

        }

        public virtual void Initialize()
        {

        }

        #endregion

        #region Static Members
        
        /// <summary>
        /// To keep all the game screens. Accessible throughout the entire game.
        /// </summary>
        public static Dictionary<string, Screen> Screens { get; set; }

        /// <summary>
        /// Returns the first screen with Enabled equals true
        /// </summary>
        public static Screen ActiveScreen
        {
            get
            {
                return Screens.Select(s => s.Value).First(s => s.Enabled);
            }
        }

        public static Screen[] GetGameScreens()
        {
            var screens = new List<Screen>();

            screens.Add(new Level { 
                Name = "Levels",
            });

            screens.Add(new LevelPicker {
                Name = "Level Picker",
            });

            return screens.ToArray();
        }

        #endregion
    }
}
