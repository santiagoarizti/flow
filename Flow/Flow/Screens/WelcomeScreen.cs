﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using Flow.Utilities;

namespace Flow.Screens
{
    public class WelcomeScreen : Screen
    {
        #region Properties

        Rectangle welcomePosition { get; set; }

        #endregion

        #region Constructors

        public WelcomeScreen(bool enabled = false) 
            : base(enabled)
        {
            
        }

        #endregion

        #region Methods

        public override void Initialize()
        {
            base.Initialize();
        }

        public override void Update(GameTime gameTime)
        {
            if (welcomePosition == Rectangle.Empty)
            {
                int w = Utils.welcome.Width;
                int h = Utils.welcome.Height;
                int x = (Size.X - w) / 2;
                int y = (Size.Y - h) / 2;
                welcomePosition = new Rectangle(x, y, w, h);
            }

            var k = Keyboard.GetState();

            if (k.IsKeyDown(Keys.NumPad1) || k.IsKeyDown(Keys.D1))
            {
                ((Level)Screen.Screens["Level"]).levelNumber = 1;
                Screen.Screens["Level"].Enable();
            }
            if (k.IsKeyDown(Keys.NumPad2) || k.IsKeyDown(Keys.D2))
            {
                ((Level)Screen.Screens["Level"]).levelNumber = 2;
                Screen.Screens["Level"].Enable();
            }
            if (k.IsKeyDown(Keys.NumPad3) || k.IsKeyDown(Keys.D3))
            {
                ((Level)Screen.Screens["Level"]).levelNumber = 3;
                Screen.Screens["Level"].Enable();
            }
            if (k.IsKeyDown(Keys.NumPad4) || k.IsKeyDown(Keys.D4))
            {
                ((Level)Screen.Screens["Level"]).levelNumber = 4;
                Screen.Screens["Level"].Enable();
            }
            if (k.IsKeyDown(Keys.NumPad5) || k.IsKeyDown(Keys.D5))
            {
                ((Level)Screen.Screens["Level"]).levelNumber = 5;
                Screen.Screens["Level"].Enable();
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            Utils.spriteBatch.Draw(Utils.welcome, welcomePosition, Color.White);

            base.Draw(gameTime);
        }

        #endregion
    }
}
