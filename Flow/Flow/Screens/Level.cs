﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.IO;
using Flow.EventHandlers;
using Flow.Utilities;
using Microsoft.Xna.Framework.Input;
using Flow.Squares;

namespace Flow.Screens
{
    class Level : Screen
    {
        #region Properties

        Square[,] SquareGrid { get; set; }
        int SquaresInRow { get; set; } // measured in squares
        float GridSizeInPixels { get; set; }
        Point GridOrigin { get; set; }

        PrintText toPrint { get; set; }

        float SquareSize; // measured in pixels

        public string LevelPath
        {
            get
            {
                return string.Format(@"C:\Users\ARIZTI\Documents\visual studio 2010\Projects\Flow\Flow\FlowContent\Levels\{0}.txt", levelNumber);
            }
        }
        string LevelLayout; // string containing square map.
        public int levelNumber { get; set; }

        Dictionary<Color, SquareLine> PaintLines { get; set; }
        List<SquarePaintedArea> StartEndSquares { get; set; }

        Square ActiveSquare;
        Square PreviousSquare;

        /// <summary>
        /// These are just the lines of the grid... nothing too important
        /// </summary>
        Rectangle[] GridBorders { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// Things that happen just at the start of the app
        /// </summary>
        public Level()
            : base()
        {
            GridSizeInPixels = 481;
            GridOrigin = new Point(9, 109);

            Player.MouseDown += OnMouseDown;
            Player.MouseUp += OnMouseUp;

            ActiveSquareChanged += OnActiveSquareChanged;
        }

        #endregion

        #region Events

        public event ActiveSquareChangedHandler ActiveSquareChanged;

        #endregion

        #region Methods

        #region Initialization

        /// <summary>
        /// Things that happen every new level
        /// </summary>
        public override void Initialize()
        {
            ActiveSquare = null;
            PreviousSquare = null;

            LoadFromFile();

            toPrint = new PrintText();

            InitializeSquares();

            InitializeGridLines();
        }

        void InitializeSquares()
        {
            var lines = LevelLayout.Split('\n');
            SquaresInRow = lines.Length;
            SquareSize = (GridSizeInPixels - 1) / SquaresInRow;
            int size = (int)Math.Round(SquareSize);
            SquareGrid = new Square[SquaresInRow, SquaresInRow];
            PaintLines = new Dictionary<Color, SquareLine>();
            StartEndSquares = new List<SquarePaintedArea>();

            for (int i = 0; i < SquaresInRow; i++) 
            for (int j = 0; j < SquaresInRow; j++)
            {
                Square currSquare = new Square(lines[j][i] == '-' ? SquareType.Normal : SquareType.StartEnd);

                currSquare.DrawLocation = new Rectangle((int)(i * (SquareSize) + 1) + GridOrigin.X, (int)(j * (SquareSize) + 1) + GridOrigin.Y, size, size);
                currSquare.GridLocation = new Point(i, j);

                if (currSquare.IsStartEnd)
                {
                    var theColor = Square.GetSquareColor(lines[j][i]);

                    StartEndSquares.Add(new SquarePaintedArea(currSquare, theColor));
                }

                // Register every square to this event:
                currSquare.DragIn += OnSquareDragIn;
                currSquare.DragOut += OnSquareDragOut;

                SquareGrid[i, j] = currSquare;
            }
        }

        /// <summary>
        /// Create grid lines coordenates.
        /// </summary>
        void InitializeGridLines()
        {
            // Create grid lines coordenates
            var points = new List<Rectangle>();

            for (float j = 0; j < SquaresInRow * SquareSize + 1; j += SquareSize)
            for (float i = 0; i < Math.Floor(SquaresInRow * SquareSize) + 1; i++) // Using math.floor because it overshot by 1 px.
            {
                points.Add(new Rectangle((int)Math.Round(i + GridOrigin.X) + 0, (int)Math.Round(j + GridOrigin.Y) + 0, 1, 1));
                points.Add(new Rectangle((int)Math.Round(j + GridOrigin.X) + 0, (int)Math.Round(i + GridOrigin.Y) + 0, 1, 1));
            }

            GridBorders = points.Distinct().ToArray();
        }

        void LoadFromFile()
        {
            using (StreamReader fileContent = new StreamReader(LevelPath))
                LevelLayout = fileContent.ReadToEnd();  
        }

        #endregion

        #region Game Loop

        public override void Update(GameTime gameTime)
        {
            // To exit to welcome window.
            if (Keyboard.GetState().IsKeyDown(Keys.R))
                Screen.Screens["Welcome"].Enable();

            Player.Update(); // TODO: problem: active square is not updated yet (it updates below) and OnMouseDown is being called.

            var someSquare = GetActiveSquare();

            // If mouse is inside of the grid
            if (someSquare != null)
            {
                // I want this: in active square always the newer square, in prev always the prev. never equal.
                if (ActiveSquare != someSquare)
                {
                    PreviousSquare = ActiveSquare;
                    ActiveSquare = someSquare;

                    // trigger event
                    ActiveSquareChanged(this, new ActiveSquareChangedEventArgs(ActiveSquare.GridLocation,
                        PreviousSquare == null ? Point.Zero : PreviousSquare.GridLocation, Player.Color));
                }
            }

            // TODO: at the moment this does nothing, in the future it can handle animations and stuff.
            foreach (var s in PaintLines.Values)
            {
                s.Update();
            }

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            DrawGridBorders();

            foreach (var s in StartEndSquares)
                s.Draw();

            foreach (var l in PaintLines)
                l.Value.Draw(gameTime);

            Player.Draw();

            toPrint.Draw();

            base.Draw(gameTime);
        }

        void DrawGridBorders()
        {
            foreach (var r in GridBorders)
                Utils.spriteBatch.Draw(Utils.whitePixel, r, Color.White);
        }

        Square GetActiveSquare()
        {
            int x = (int)Math.Floor(((double)Player.CurrentPoint.X - 1 - GridOrigin.X) / (SquareSize));
            int y = (int)Math.Floor(((double)Player.CurrentPoint.Y - 1 - GridOrigin.Y) / (SquareSize));

            if (x < 0 || x >= SquaresInRow || y < 0 || y >= SquaresInRow) return null;

            return SquareGrid[x, y];
        }

        bool IsTheLastOfAnyLineOrStartEnd(Square square, out Color color)
        {
            foreach (var s in StartEndSquares)
            {
                if (s.Square == square)
                {
                    color = s.Color;
                    return true;
                }
            }
            foreach (var a in PaintLines)
            {
                if (a.Value.Squares.Last().Square == square)
                {
                    color = a.Key;
                    return true;
                }
            }
            color = Color.Transparent;
            return false;
        }

        #endregion

        #endregion

        #region EventHandlers

        void OnSquareDragIn(object sender, DragInEventArgs e)
        {
            var theSquare = (Square)sender;

            PaintLines[e.Color].AddSquare((Square)sender);
        }

        void OnSquaresChanged(object sender, SquaresChangedEventArgs e)
        {
            toPrint.UpdateText("Sq Line", e.Squares.Select(s => s.Square.GridLocation.ToString()).Aggregate((a, b) => a + "\n" + b));
        }

        void OnSquareDragOut(object sender, DragOutEventArgs e)
        {

        }

        void OnActiveSquareChanged(object sender, ActiveSquareChangedEventArgs e)
        {
            // decide if paint must be placed
            if (e.PlayerBrush != Color.Transparent)
            {
                Point prev = e.PrevSquare;
                Point curr = prev;
                
                while (curr != e.CurrSquare) // This will happen for every square in between active and previous. Most times it is only 1. but when player moved too fast it is more than 1
                {
                    if (e.CurrSquare.X != prev.X)
                    {
                        bool currIsLeft = e.CurrSquare.X < prev.X;
                        curr.X = currIsLeft ? curr.X - 1 : curr.X + 1;

                        SquareGrid[curr.X, curr.Y].TriggerDragIn(new DragInEventArgs(e.PlayerBrush));
                        SquareGrid[prev.X, prev.Y].TriggerDragOut(new DragOutEventArgs());
                        prev = curr;
                    }

                    if (e.CurrSquare.Y != prev.Y)
                    {
                        bool currIsAbove = e.CurrSquare.Y < prev.Y;
                        curr.Y = currIsAbove ? curr.Y - 1 : curr.Y + 1;

                        SquareGrid[curr.X, curr.Y].TriggerDragIn(new DragInEventArgs(e.PlayerBrush));
                        SquareGrid[prev.X, prev.Y].TriggerDragOut(new DragOutEventArgs());
                        prev = curr;
                    }
                }
            }
            
            //Print to screen
            var tempSquare = SquareGrid[e.CurrSquare.X, e.CurrSquare.Y];
            toPrint.UpdateText("Curr Square", string.Format("{0}, {1}", e.CurrSquare.ToString(), "not implemented")); //tempSquare.IsPainted ? "F: " + tempSquare.PaintedArea.DirectionFrom.ToString() + ". T: " + tempSquare.PaintedArea.DirectionTo.ToString() : "Not Painted"));
        }

        void OnMouseDown(ClickingEventArgs e)
        {
            toPrint.UpdateText("Is Clicking", "Yes");

            Color theColor;

            if (IsTheLastOfAnyLineOrStartEnd(ActiveSquare, out theColor))
            {
                Player.Color = theColor;

                if (ActiveSquare.IsStartEnd)
                {
                    PaintLines[theColor] = new SquareLine(theColor);
                    PaintLines[theColor].SquaresChanged += OnSquaresChanged;
                }

                ActiveSquare.TriggerDragIn(new DragInEventArgs(theColor, true));
            }
        }

        void OnMouseUp(ClickingEventArgs e)
        {
            toPrint.UpdateText("Is Clicking", "No");

            Player.Color = Color.Transparent;
        }

        #endregion

        #region Static Members

        #endregion
    }
}
