﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Flow.Utilities
{
    class PrintText
    {
        public StringBuilder TextToDraw { get; set; }
        public Vector2 TextStart { get; set; }
        public static SpriteFont Font { get; set; }

        Dictionary<string, string> testStrings { get; set; }

        public PrintText()
        {
            TextStart = new Vector2(5);

            testStrings = new Dictionary<string, string>();

            TextToDraw = new StringBuilder();
        }

        public void UpdateText(string key, string value)
        {
            testStrings[key] = value;

            TextToDraw = new StringBuilder();

            foreach (var s in testStrings)
            {
                TextToDraw.AppendFormat("{0}: {1}\n", s.Key, s.Value);
            }
        }

        public void UpdateText()
        {
            TextToDraw = new StringBuilder();

            foreach (var s in testStrings)
            {
                TextToDraw.AppendFormat("{0}: {1}\n", s.Key, s.Value);
            }
        }
        
        public void Draw()
        {
            Utils.spriteBatch.DrawString(Font, TextToDraw, TextStart, Color.White);
        }
    }
}
