﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.Utilities
{
    public enum SquareType
    {
        StartEnd,
        Normal
    }

    public enum SquareSituations
    {
        sr, su, sl, sd, sn,
        ur, ud, ul,
        dr, lr, dl,
        un, dn, rn, ln,
        nn
    }

    public enum PaintDirection
    {
        Up = 1,      // 00001
        Down = 2,    // 00010
        Left = 4,    // 00100
        Right = 8,   // 01000
        None = 16    // 10000
        // I added numbers like that to be able to use the or operator (|)
    }
}
