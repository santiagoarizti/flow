﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Flow.EventHandlers;

namespace Flow.Utilities
{
    public class Utils
    {
        public static SpriteBatch spriteBatch { get; set; }
        public static GameTime gameTime { get; set; }
        public static Texture2D whitePixel { get; set; }
        public static MouseState currMouse { get; set; }
        public static MouseState prevMouse { get; set; }
        public static Texture2D welcome { get; set; }
    }
}
