using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;
using System.Text;
using System.IO;
using Flow.EventHandlers;
using Flow.Screens;
using Flow.Utilities;
using Flow.Squares;

namespace Flow
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            Screen.Screens = new Dictionary<string,Screen>();

            Screen.Screens["Level"] =new Level { 
                Name = "Level",
            };
            Screen.Screens["Level Picker"] = new LevelPicker {
                Name = "Level Picker",
            };
            Screen.Screens["Welcome"] =new WelcomeScreen(true) { 
                Name = "Welcome",
            };

            Screen tempScreen = Screen.ActiveScreen;

            Square.SetSquareColors();

            graphics.PreferredBackBufferWidth = (int)(tempScreen.Size.X); 
            graphics.PreferredBackBufferHeight = (int)(tempScreen.Size.Y); 
            graphics.ApplyChanges();

            Square.SquareTextures = new Dictionary<SquareSituations, Texture2D>();

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            Utils.spriteBatch = new SpriteBatch(GraphicsDevice);

            foreach (SquareSituations s in Enum.GetValues(typeof(SquareSituations)))
            {
                Square.SquareTextures.Add(s, Content.Load<Texture2D>(s.ToString()));
            }

            Utils.whitePixel = Content.Load<Texture2D>("white_pixel");

            PrintText.Font = Content.Load<SpriteFont>("sprite_font");

            Utils.welcome = Content.Load<Texture2D>("welcome");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            Content.Unload();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            Utils.currMouse = Mouse.GetState();

            Screen.ActiveScreen.Update(gameTime);

            Utils.prevMouse = Utils.currMouse;

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Screen.ActiveScreen.BackgroundColor);

            Utils.spriteBatch.Begin();

            Screen.ActiveScreen.Draw(gameTime);

            Utils.spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
