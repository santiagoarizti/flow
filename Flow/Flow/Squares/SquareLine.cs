﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Flow.Utilities;
using Flow.EventHandlers;

namespace Flow.Squares
{
    public class SquareLine
    {
        #region Properties

        public List<SquarePaintedArea> Squares { get; set; }

        Color Color { get; set; }

        #endregion

        #region Events

        public event SquaresChangedHandler SquaresChanged;

        #endregion

        #region Constructors

        public SquareLine(Color theColor)
        {
            Color = theColor;
            Squares = new List<SquarePaintedArea>();
        }

        #endregion

        #region Methods

        public void AddSquare(Square square)
        {
            var sss = Squares.FirstOrDefault(s => s.Square == square);
            if (sss != null)
            {
                var oldSquares = Squares.GetRange(Squares.IndexOf(sss), Squares.Count - Squares.IndexOf(sss));
                oldSquares.ForEach(s => Squares.Remove(s));
            }
                
            var prev = Squares.LastOrDefault();

            Squares.Add(new SquarePaintedArea(square, Color));

            if (prev != null) UpdatePaint(prev);
            UpdatePaint(Squares.Last());

            SquaresChanged(this, new SquaresChangedEventArgs(Squares));
        }

        void UpdatePaint(SquarePaintedArea squareP)
        {
            Point currSquare = squareP.Square.GridLocation;
            int index = Squares.IndexOf(squareP);

            // FROM

            var dirFrom = PaintDirection.None;
            Point prevSquare = currSquare;

            try { prevSquare = Squares[index - 1].Square.GridLocation; }
            catch { }

            if (currSquare.Y != prevSquare.Y)
                dirFrom = currSquare.Y < prevSquare.Y ? PaintDirection.Down : PaintDirection.Up;
            if (currSquare.X != prevSquare.X)
                dirFrom = currSquare.X < prevSquare.X ? PaintDirection.Right : PaintDirection.Left;

            squareP.DirectionFrom = dirFrom;

            // TO

            var dirTo = PaintDirection.None;
            Point nextSquare = currSquare;

            try { nextSquare = Squares[index + 1].Square.GridLocation; }
            catch { }

            if (currSquare.Y != nextSquare.Y)
                dirTo = currSquare.Y < nextSquare.Y ? PaintDirection.Down : PaintDirection.Up;
            if (currSquare.X != nextSquare.X)
                dirTo = currSquare.X < nextSquare.X ? PaintDirection.Right : PaintDirection.Left;

            squareP.DirectionTo = dirTo;
        }

        public void Update()
        {
        }

        public void Draw(GameTime gameTime)
        {
            foreach (var s in Squares)
            {
                s.Draw();
            }
        }

        #endregion

        #region Event Handlers

        #endregion
    }
}
