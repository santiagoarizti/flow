﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Flow.Utilities;

namespace Flow.Squares
{
    public class SquarePaintedArea
    {
        #region Properties

        public Color Color;
        public PaintDirection DirectionFrom;
        public PaintDirection DirectionTo;
        public Square Square;

        public SquareSituations Situation
        {
            get
            {
                SquareSituations sS = SquareSituations.nn;

                switch (DirectionFrom | DirectionTo)
                {
                    case PaintDirection.Down:
                    case PaintDirection.Up:
                    case PaintDirection.Right:
                    case PaintDirection.Left:
                    case PaintDirection.None:
                        sS = Square.IsStartEnd ? SquareSituations.sn : SquareSituations.nn; //not always
                        break;
                    case PaintDirection.Up | PaintDirection.Right:
                        sS = SquareSituations.ur;
                        break;
                    case PaintDirection.Up | PaintDirection.Down:
                        sS = SquareSituations.ud;
                        break;
                    case PaintDirection.Up | PaintDirection.Left:
                        sS = SquareSituations.ul;
                        break;
                    case PaintDirection.Down | PaintDirection.Right:
                        sS = SquareSituations.dr;
                        break;
                    case PaintDirection.Left | PaintDirection.Right:
                        sS = SquareSituations.lr;
                        break;
                    case PaintDirection.Down | PaintDirection.Left:
                        sS = SquareSituations.dl;
                        break;
                    case PaintDirection.Up | PaintDirection.None:
                        sS = Square.IsStartEnd ? SquareSituations.su : SquareSituations.un;
                        break;
                    case PaintDirection.Down | PaintDirection.None:
                        sS = Square.IsStartEnd ? SquareSituations.sd : SquareSituations.dn;
                        break;
                    case PaintDirection.Right | PaintDirection.None:
                        sS = Square.IsStartEnd ? SquareSituations.sr : SquareSituations.rn;
                        break;
                    case PaintDirection.Left | PaintDirection.None:
                        sS = Square.IsStartEnd ? SquareSituations.sl : SquareSituations.ln;
                        break;
                }
                

                return sS;
            }
        }

        #endregion

        #region Constructor

        public SquarePaintedArea(Square square, Color color)
        {
            Color = color;
            Square = square;
            DirectionFrom = PaintDirection.None;
            DirectionTo = PaintDirection.None;
        }

        #endregion

        #region Methods

        public void Draw()
        {
            Utils.spriteBatch.Draw(Square.SquareTextures[Situation], Square.DrawLocation, Color);
        }

        #endregion
    }
}
