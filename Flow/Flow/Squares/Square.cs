﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Flow.Utilities;
using Flow.EventHandlers;

namespace Flow.Squares
{
    public class Square
    {
        #region Properties

        public bool IsNormal { get { return SqType == SquareType.Normal; } }
        public bool IsStartEnd { get { return !IsNormal; } }
        SquareType SqType;
        
        public Rectangle DrawLocation { get; set; }

        /// <summary>
        /// Not in pixels, but location in square count.
        /// </summary>
        public Point GridLocation { get; set; }

        #endregion

        #region Events

        public event DragInHandler DragIn;
        public event DragOutHandler DragOut;

        #endregion

        #region Constructors

        public Square(SquareType sqType = SquareType.Normal)
        {
            SqType = sqType;
        }

        #endregion

        #region Methods

        /// <summary>
        /// It tells the square that the player just left it while dragging.
        /// The square should take meassures to paint correctly
        /// </summary>
        public void TriggerDragOut(DragOutEventArgs e)
        {
            DragOut(this, e);
        }

        /// <summary>
        /// It tells the square that the player just left it while dragging.
        /// The square should take meassures to paint correctly
        /// </summary>
        public void TriggerDragIn(DragInEventArgs e)// PaintDirection whichBorder, Color color, bool lineStartedHere = false)
        {
            DragIn(this, e);
        }

        #endregion

        #region Event Handlers
        
        #endregion

        #region Static Members

        public static Dictionary<SquareSituations, Texture2D> SquareTextures { get; set; }

        static Dictionary<char, Color> SquareColors { get; set; }

        public static void SetSquareColors()
        {
            SquareColors = new Dictionary<char, Color>();

            SquareColors['R'] = Color.Red;
            SquareColors['G'] = Color.Green;
            SquareColors['B'] = Color.Blue;
            SquareColors['Y'] = Color.Yellow;
            SquareColors['O'] = Color.Orange;
            SquareColors['M'] = Color.Magenta;
            SquareColors['L'] = Color.LimeGreen;
            SquareColors['g'] = Color.LightGray;
        }

        public static Color GetSquareColor(char relatedChar)
        {
            // If the color is not stored, create a random one and create it.
            if (!SquareColors.ContainsKey(relatedChar))
            {
                var r = new Random();
                SquareColors[relatedChar] = new Color(r.Next(0, 255), r.Next(0, 255), r.Next(0, 255));
            }

            return SquareColors[relatedChar];
        }

        #endregion
    }
}
