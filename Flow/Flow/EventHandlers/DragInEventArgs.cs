﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Utilities;
using Microsoft.Xna.Framework;

namespace Flow.EventHandlers
{
    public delegate void DragInHandler(object sender, DragInEventArgs e);

    public class DragInEventArgs
    {
        //public PaintDirection WhichBorder { get; set; }
        public Color Color { get; set; }
        public bool LineStartedHere { get; set; }

        public DragInEventArgs(Color color, bool lineStartedHere = false)
        {
            //WhichBorder = whichBorder;
            Color = color;
            LineStartedHere = lineStartedHere;
        }
    }
}
