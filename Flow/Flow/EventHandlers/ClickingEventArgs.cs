﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow.EventHandlers
{
    public delegate void ClickedHandler(ClickingEventArgs e);

    public class ClickingEventArgs : EventArgs
    {
        public bool IsClicking { get; set; }

        public ClickingEventArgs(bool isClicking)
        {
            IsClicking = isClicking;
        }
    }
}
