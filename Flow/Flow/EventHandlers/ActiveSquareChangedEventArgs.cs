﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace Flow.EventHandlers
{
    public delegate void ActiveSquareChangedHandler(object sender, ActiveSquareChangedEventArgs e);

    public class ActiveSquareChangedEventArgs : EventArgs
    {
        public Point PrevSquare { get; set; }
        public Point CurrSquare { get; set; }
        public Color PlayerBrush { get; set; }

        public ActiveSquareChangedEventArgs(Point curr, Point prev, Color playerBrush)
        {
            PrevSquare = prev;
            CurrSquare = curr;
            PlayerBrush = playerBrush;
        }
    }
}
