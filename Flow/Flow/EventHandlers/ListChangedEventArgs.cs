﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Flow.Squares;

namespace Flow.EventHandlers
{
    public delegate void SquaresChangedHandler(object sender, SquaresChangedEventArgs e);

    public class SquaresChangedEventArgs
    {
        public List<SquarePaintedArea> Squares;

        public SquaresChangedEventArgs(List<SquarePaintedArea> squares)
        {
            Squares = squares;
        }
    }
}
